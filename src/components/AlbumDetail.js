import React from 'react';
import { Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import { Image } from "react-native";


const AlbumDetail = ({album}) => {
    const { artist, title, thumbnail_image, image } = album;
    return (
        // <Content>
        //     <Card>
        //         <CardItem>
        //             <Left>
        //                 <Thumbnail source={{uri: thumbnail_image}} />
        //                 <Body>
        //                 <Text>{artist}</Text>
        //                 <Text note>{title}</Text>
        //                 </Body>
        //             </Left>
        //         </CardItem>
        //         <CardItem cardBody>
        //             {/* <Image source={{uri: image}} style={{height: 200, width: null, flex: 1}}/> */}

        //         </CardItem>
        //     </Card>
        // </Content>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: thumbnail_image}} />
                <Body>
                  <Text>{artist}</Text>
                  <Text note>{title}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: image}} style={{height: 200, width: null, flex: 1}} />
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
        </Content>
    )
};

export default AlbumDetail