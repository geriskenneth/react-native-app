import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import  AlbumDetail  from './AlbumDetail';

class AlbumList extends Component {
    state = { albums: [] };

    componentWillMount() {
        fetch('https://rallycoding.herokuapp.com/api/music_albums')
        .then(response => response.json())
        .then(data => this.setState({ albums: data }));
    }

    renderAlbums() {
        return this.state.albums.map(album => 
            <AlbumDetail key={album.title} album={album} /> 
        )
    }
    
    render() {
        return (
            <Container>
                <Content padder>
                {this.renderAlbums()}
                </Content>        
            </Container>
        );
    }
}

export default AlbumList