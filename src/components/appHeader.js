import React from 'react';
import { Header, Left, Body, Right, Title, Button, Icon } from 'native-base';

const appHeader = (props) => {
    return (
        <Header>
            <Left>
                <Button transparent>
                    <Icon name="arrow-back" />
                </Button>
            </Left>
            <Body>
                <Title>{props.headerText}</Title>
            </Body>
            <Right />
        </Header>
    );
}

export default appHeader

