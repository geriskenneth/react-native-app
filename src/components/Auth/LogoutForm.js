import React, { Component } from 'react';
import { Button, Content, Text, Container} from 'native-base';
import firebase from 'firebase';

export default class LogoutForm extends Component {
    render() {
        return (
            <Container>
                <Content style={{ padding: 10}}>
                    <Button onPress={() => firebase.auth().signOut()}>
                            <Text>Log out</Text>
                    </Button>
                </Content>
            </Container>
        )
    }
}