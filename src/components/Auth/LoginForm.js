import React, { Component } from 'react';
import { Button, Content, Form, Item, Input, Text, Container , Spinner} from 'native-base';
import firebase from 'firebase';

export default class LoginForm extends Component {
    state = { email: '', password: '', error: '', showToast: false, loading: false};

    onButtonPress() {
        const { email, password } = this.state;
        this.setState({error: '', loading: true});

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(this.onLoginSuccess.bind(this))
        .catch(() => {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(this.onLoginFail.bind(this))
        });
    }
    onLoginFail() {
        this.setState({error: 'Authentication Failed', loading: false})
    }
    onLoginSuccess() {
        this.setState({
            email: '', 
            password: '',
            loading: false,
            error: ''
        })
    }


    renderButton() {
        if(this.state.loading){
            return <Spinner />
        }
        return(
            <Button block onPress={this.onButtonPress.bind(this)}>
                <Text>Log in</Text>
            </Button>
        )
    }
    render() {
        return (
            <Container>
                <Content style={{ padding: 10}}>
                <Form>
                    <Item>
                    <Input placeholder="Email" autoCapitalize='none' value={this.state.email} onChangeText={email => this.setState({email})}/>
                    </Item>
                    <Item last>
                    <Input placeholder="Password" secureTextEntry autoCapitalize='none' value={this.state.password} onChangeText={password => this.setState({password})}/>
                    </Item>
                    <Text warning>{this.state.error}</Text>
                </Form>
                {this.renderButton()}
                </Content>
            </Container>
        );
    }
}

