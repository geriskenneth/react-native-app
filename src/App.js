import React, { Component } from 'react';
import firebase from 'firebase';
import { Root, Button, Text, Spinner } from 'native-base';
import Header from './components/AppHeader';
// import AlbumList from './components/AlbumList';
import LoginForm from './components/Auth/LoginForm';
import LogoutForm from './components/Auth/LogoutForm';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers  from './reducers/';
export default class App extends Component {
    state = { loggedIn: null };

    componentWillMount() {
        console.log(Provider);
        firebase.initializeApp({
            apiKey: "AIzaSyD4f3gF5qP063rBKXx7jlXNXh8oNfJWQAA",
            authDomain: "react-native-efdcc.firebaseapp.com",
            databaseURL: "https://react-native-efdcc.firebaseio.com",
            projectId: "react-native-efdcc",
            storageBucket: "react-native-efdcc.appspot.com",
            messagingSenderId: "358191355783"
        });

        firebase.auth().onAuthStateChanged((user) => {
            if(user) {
                this.setState({loggedIn: true})
            }else{
                this.setState({loggedIn: false})
            }
            // user ? this.setState({loggedIn: true}) : this.setState({loggedIn: false})
        })
    }

    renderContent() {
        switch(this.state.loggedIn){
            case true:             
                return <LogoutForm />
            case false: 
                return <LoginForm />
            default: <Spinner />
        }
    }
    render() {
        return (
            
            <Provider store={createStore(reducers)}>
                <Root >
                    <Header headerText="hallo"/>
                    {this.renderContent()}
                </Root>
            </Provider>
        )
    }
}
